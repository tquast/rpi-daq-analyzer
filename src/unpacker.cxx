/*  
created: 22 April 2020
This executable was implemented by Thorben Quast, thorben.quast@cern.ch.
Its sole purpose is the unpacking of raw skiroc2-cms data into ROOT TTrees for further (python-based) analysis.
Other functionality included in this program was implemented by Arnaud Steen, arnaud.steen@cern.ch
*/

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <stdio.h>
#include <limits>
#include <boost/program_options.hpp>

#include "unpacker.h"
#include "ntupler.h"


int main(int argc, char**argv)
{
  uint32_t m_headerSize;
  uint32_t m_eventTrailerSize;
  uint32_t m_nSkipEvents;
  uint32_t m_maxEvents;
  bool m_compressedData;
  std::string m_fileName, m_outputName, m_outputDir, m_hexaboardType;
  try {
    /** Define and parse the program options
     */
    namespace po = boost::program_options;
    po::options_description generic_options("Generic options");
    generic_options.add_options()
    ("help,h", "Print help messages")
    ("fileName,f", po::value<std::string>(&m_fileName)->default_value("moduletest.raw"), "name of input file")
    ("outputDir,O", po::value<std::string>(&m_outputDir)->default_value("results"), "name of output directory where output files will be stored")
    ("outputName,o", po::value<std::string>(&m_outputName)->default_value("moduletest"), "prefix name to build output files");

    po::options_description daq_options("DAQ options");
    daq_options.add_options()
    ("headerSize", po::value<uint32_t>(&m_headerSize)->default_value(192), "size (bytes) of the header")
    ("eventTrailerSize", po::value<uint32_t>(&m_eventTrailerSize)->default_value(2), "size (bytes) of the event trailer")
    ("hexaboardType", po::value<std::string>(&m_hexaboardType)->default_value("8inch"), "flag to know if the data comes from 6 or 8 inch sk2cms module (important for the mapping)")
    ("compressedData", po::value<bool>(&m_compressedData)->default_value(true), "option to set if the data have been compressed");

    po::options_description analysis_options("Global analysis options");
    analysis_options.add_options()
    ("maxEvents,N", po::value<uint32_t>(&m_maxEvents)->default_value(std::numeric_limits<uint32_t>::max()), "maximum number of event to process")
    ("nSkipEvents,n", po::value<uint32_t>(&m_nSkipEvents)->default_value(1), "number of event to skip (better to skip 1st event)");

    po::options_description cmdline_options;
    cmdline_options.add(generic_options).add(daq_options).add(analysis_options);

    po::variables_map vm;
    try {
      po::store(po::parse_command_line(argc, argv, cmdline_options),  vm);
      if ( vm.count("help")  ) {
        std::cout << generic_options   << std::endl;
        std::cout << daq_options       << std::endl;
        std::cout << analysis_options  << std::endl;
        return 0;
      }
      po::notify(vm);
    }
    catch (po::error& e) {
      std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
      std::cerr << generic_options << std::endl;
      return 1;
    }
    
    std::cout << std::endl;
    if ( vm.count("fileName") )          std::cout << "fileName = "           << m_fileName << std::endl;
    if ( vm.count("outputDir") )         std::cout << "outputDir = "          << m_outputDir << std::endl;
    if ( vm.count("outputName") )        std::cout << "outputName = "         << m_outputName << std::endl;
    std::cout << std::endl;
    if ( vm.count("headerSize") )        std::cout << "m_headerSize = "       << m_headerSize << std::endl;
    if ( vm.count("eventTrailerSize") )  std::cout << "eventTrailerSize = "   << m_eventTrailerSize << std::endl;
    if ( vm.count("compressedData") )    std::cout << "compressedData= "      << m_compressedData << std::endl;
    std::cout << std::endl;

    if ( vm.count("maxEvents") )         std::cout << "maxEvents= "           << m_maxEvents << std::endl;
    if ( vm.count("nSkipEvents") )       std::cout << "nSkipEvents = "        << m_nSkipEvents << std::endl;
    if ( vm.count("hexaboardType") && (!(m_hexaboardType == "8inch" || m_hexaboardType == "6inch")) ) {
      std::cout << "Invalid choice for option 'hexaboardType'. Allowed choices : '6inch', '8inch'" << std::endl;
      throw po::validation_error(po::validation_error::invalid_option_value, "hexaboardType");
    }
    if ( vm.count("hexaboardType") )     std::cout << "hexaboardType = "      << m_hexaboardType << std::endl;
    std::cout << std::endl;
  }
  catch (std::exception& e) {
    std::cerr << "Unhandled Exception reached the top of main: "
              << e.what() << ", application will now exit" << std::endl;
    return 2;
  }

  unpacker _unpacker(m_fileName,
                     m_headerSize,
                     m_eventTrailerSize,
                     m_nSkipEvents,
                     m_maxEvents,
                     m_compressedData,
                     m_hexaboardType);

  unsigned int m_event = 0;

  auto _ntupler = std::make_shared<rawntupler>(m_outputDir, m_outputName, std::string("sk2cms"), std::string("sk2cms hexaboard raw data tree"));
  while (1) {
    std::vector<skiroc2cms> skirocs;
    if ( !_unpacker.unpack(skirocs) || m_event >= m_maxEvents )
      break;
    else {
      bool checker = true;
      for ( auto skiroc : skirocs ) {
        checker = skiroc.check(0);
        if (checker == false) {
          std::cout << "Event " << std::dec << m_event << " has data corruption -> it will be skipped " << std::endl;
          break;
        }
      }
      if (checker == false) {
        m_event++;
        continue; //we skip the event if a data corruption is found
      }
      if ( m_event % 100 == 0 ) std::cout << "Processed " << std::dec << m_event << " events\t" << skirocs.size() << std::endl;
      _ntupler->fill(skirocs);
      m_event++;
    }
  }


}
