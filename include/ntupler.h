#include <iostream>
#include <map>
#include <TFile.h>
#include <TTree.h>

#include "skiroc2cms.h"

class ntupler
{
 public:
  ntupler();
  ntupler(std::string outputDir, std::string outputname, std::string treename, std::string description);
  ~ntupler();
 protected:
  TFile* m_file;
  TTree* m_outtree;
  std::map<int,int> m_padMap;
};

class rawntupler : public ntupler
{ 
 public:
  rawntupler();
  rawntupler(std::string outputDir, std::string outputname, std::string treename, std::string description);

  void fill(std::vector<skiroc2cms> &skirocs);

 private:
  int m_event;
  int m_chip;
  int m_roll;
  int m_dacinj;
  int m_timestamp[NUMBER_OF_SCA];
  int m_hg[NUMBER_OF_SCA][N_CHANNELS_PER_SKIROC];
  int m_lg[NUMBER_OF_SCA][N_CHANNELS_PER_SKIROC];
  int m_tot_fast[N_CHANNELS_PER_SKIROC];
  int m_tot_slow[N_CHANNELS_PER_SKIROC];
  int m_toa_rise[N_CHANNELS_PER_SKIROC];
  int m_toa_fall[N_CHANNELS_PER_SKIROC];

};