#include <sstream>
#include <cstring>
#include "ntupler.h"

ntupler::ntupler()
{
  m_file=NULL;
  m_outtree=NULL;
}

ntupler::ntupler(std::string outputDir, std::string outputname, std::string treename, std::string description)
{
  std::ostringstream os(std::ostringstream::ate);
  os.str("");
  os << outputDir << "/" << outputname;
  m_file=new TFile(os.str().c_str(),"RECREATE");
  m_outtree=new TTree(treename.c_str(),description.c_str());
}

ntupler::~ntupler()
{
  if( NULL!=m_file ){
    m_file->Write();
    m_file->Close();
  }
}

rawntupler::rawntupler() :
  ntupler()
{;}

rawntupler::rawntupler(std::string outputDir, std::string outputname, std::string treename, std::string description) : 
  ntupler(outputDir, outputname, treename, description) 
{
  m_outtree->Branch("event", &m_event);
  m_outtree->Branch("chip", &m_chip);
  m_outtree->Branch("roll", &m_roll);
  m_outtree->Branch("dacinj", &m_dacinj);
  m_outtree->Branch("timestamp", &m_timestamp,"timestamp[13]/I");
  m_outtree->Branch("hg", &m_hg,"hg[13][64]/I");
  m_outtree->Branch("lg", &m_lg,"lg[13][64]/I");
  m_outtree->Branch("tot_fast", &m_tot_fast,"tot_fast[64]/I");
  m_outtree->Branch("tot_slow", &m_tot_slow,"tot_slow[64]/I");
  m_outtree->Branch("toa_rise", &m_toa_rise,"toa_rise[64]/I");
  m_outtree->Branch("toa_fall", &m_toa_fall,"toa_fall[64]/I");

  m_event=0;
}

void rawntupler::fill(std::vector<skiroc2cms>& skirocs)
{
  int iski=0;
  for( auto skiroc : skirocs ){
    m_chip=iski;
    m_roll=skiroc.rollMask();
    //m_dacinj=skiroc.dacInjection();
    std::vector<int> ts=skiroc.rollPositions();
    int its=0;
    for( std::vector<int>::iterator it=ts.begin(); it!=ts.end(); ++it ){
      m_timestamp[its]=(*it);
      its++;
    }
    for( size_t ichan=0; ichan<N_CHANNELS_PER_SKIROC; ichan++ ){
      for( size_t it=0; it<NUMBER_OF_SCA; it++ ){
	m_hg[it][ichan]=skiroc.ADCHigh(ichan,it);
	m_lg[it][ichan]=skiroc.ADCLow(ichan,it);
      }
      m_tot_fast[ichan]=skiroc.TOTFast(ichan);
      m_tot_slow[ichan]=skiroc.TOTSlow(ichan);
      m_toa_rise[ichan]=skiroc.TOARise(ichan);
      m_toa_fall[ichan]=skiroc.TOAFall(ichan);
    }
    m_outtree->Fill();
    iski++;
  }
  m_event++;
}
