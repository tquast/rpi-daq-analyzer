#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <iomanip>
#include <vector>

class skiroc2cms;

class unpacker
{
 public:
  unpacker(){;}
  unpacker(std::string fileName, unsigned int headerSize,unsigned int eventTrailerSize, unsigned int nSkipEvents, unsigned int maxEvents, bool compressedData=true, std::string hexaboardType=std::string("6inch"));
  ~unpacker(){;}

  bool unpack(std::vector<skiroc2cms> &skirocs);
  
 private:
  std::string m_fileName;
  unsigned int m_headerSize;
  unsigned int m_eventTrailerSize;
  unsigned int m_nSkipEvents;
  unsigned int m_maxEvents;
  bool m_compressedData;

  
  std::fstream m_inputstream;
  unsigned int m_event;
  char* m_buffer;
  unsigned int m_nWords;
  int m_sktoIC[4]={0,1,2,3};
};
