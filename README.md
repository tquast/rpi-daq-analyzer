# Original code base
***Forked from A. Steen's [rpi-daq-analyzer repository](https://gitlab.cern.ch/cms-hgcal-tb/rpi-daq-analyzer/).***

# Download 
```
git clone https://gitlab.cern.ch/tquast/rpi-daq-analyzer.git
```

# Prerequisites

- cmake (version 3)
- compiler supporting the C++11 standard
- ROOT
- boost libraries

# Compilation with cmake

```
mkdir build
cd build
cmake ../
make install
```

# Running the unpacker
Print all available options:

```
./bin/unpacker --help
```

Example command: 
```
./build/unpacker --fileName test/test_data.raw --outputDir test --outputName test_unpacked.root --headerSize 192 --eventTrailerSize 2 --hexaboardType 6inch --compressedData 1 --nSkipEvents 0
```